<?php

namespace App\Console\Commands;

use App\Mail\FormEnabled;
use App\Models\Config;
use App\Models\EmailReceiver;
use App\Models\EmailReciever;
use App\Services\ParseService;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ParseSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:site';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse selected site.';

    private $parseService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ParseService $parseService)
    {
        parent::__construct();
        $this->parseService = $parseService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = Config::where('key', '=', 'url')->first();
        $options = [];
        $html = $this->parseService->requestHtmlByUrl($url->value, ParseService::TYPE_GET, $options);
        $selector = Config::where('key', '=', 'selector')->first();
        $menuHtml = $this->parseService->getHtmlBySelector($html, $selector->value);
        if(!empty($menuHtml['length'])) {
            $receivers = EmailReceiver::all()->toArray();
            $receivers = array_column($receivers, 'email');
            Mail::to($receivers)
                ->send(new FormEnabled());
        }
    }
}
