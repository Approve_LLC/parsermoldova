<?php

namespace App\Services;

use DOMDocument;
use DOMXPath;

/**
 * Class ParseService
 * @package App\Services
 */
class ParseService
{

    const TYPE_GET = 'get';
    const TYPE_POST = 'post';

    /**
     * @param $url
     * @param string $type
     * @param array $options
     * @return bool|\Psr\Http\Message\StreamInterface
     */
    public function requestHtmlByUrl($url, $type = self::TYPE_GET, $options = [])
    {
        $client = new \GuzzleHttp\Client();
        switch ($type) {
            case self::TYPE_GET:
                $res = $client->get($url, $options);
                break;
            case self::TYPE_POST:
                $res = $client->post($url, $options);
                break;
            default:
                $res = $client->get($url, $options);
                break;
        }

        if ($res->getStatusCode() == \Illuminate\Http\Response::HTTP_OK) {
            return $res->getBody()->getContents();
        }

        return false;
    }

    /**
     * @param $htmlStr
     * @param $selector
     * @return mixed
     */
    public function getHtmlBySelector($htmlStr, $selector)
    {
        $dom = new DOMDocument;
        $dom->loadHTML($htmlStr);

        $xpath = new DOMXPath($dom);

        $query = '//*[text()[contains(normalize-space(.),"' . $selector . '")]]';//"script[@src='../../Skrypty/are_cookies_enabled.js']";////'*[text()="Виза"]';

        return $xpath->query($query);
    }
}
