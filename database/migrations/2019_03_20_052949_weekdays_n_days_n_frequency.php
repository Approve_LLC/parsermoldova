<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WeekdaysNDaysNFrequency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekdays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });
        Schema::create('hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });
        Schema::create('frequency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
        });

        Schema::create('schedule_config_weekday', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('schedule_config_id')->unsigned();
            $table->bigInteger('weekday_id')->unsigned();
        });
        Schema::create('schedule_config_hour', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('schedule_config_id')->unsigned();
            $table->bigInteger('hour_id')->unsigned();
        });
        Schema::create('schedule_config_frequency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('schedule_config_id')->unsigned();
            $table->bigInteger('frequency_id')->unsigned();
        });

        Schema::table('schedule_config_weekday', function (Blueprint $table) {
            $table->foreign('schedule_config_id')->references('id')->on('schedule_config')
                ->onDelete('cascade');
            $table->foreign('weekday_id')->references('id')->on('weekdays')
                ->onDelete('cascade');
        });
        Schema::table('schedule_config_hour', function (Blueprint $table) {
            $table->foreign('schedule_config_id')->references('id')->on('schedule_config')
                ->onDelete('cascade');
            $table->foreign('hour_id')->references('id')->on('hours')
                ->onDelete('cascade');
        });
        Schema::table('schedule_config_frequency', function (Blueprint $table) {
            $table->foreign('schedule_config_id')->references('id')->on('schedule_config')
                ->onDelete('cascade');
            $table->foreign('frequency_id')->references('id')->on('frequency')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_config_weekday', function (Blueprint $table) {
            $table->dropForeign('schedule_config_id');
            $table->dropForeign('weekday_id');
        });
        Schema::table('schedule_config_hour', function (Blueprint $table) {
            $table->dropForeign('schedule_config_id');
            $table->dropForeign('hour_id');
        });
        Schema::table('schedule_config_frequency', function (Blueprint $table) {
            $table->dropForeign('schedule_config_id');
            $table->dropForeign('frequency_id');
        });

        Schema::dropIfExists('schedule_config_weekday');
        Schema::dropIfExists('schedule_config_hour');
        Schema::dropIfExists('schedule_config_frequency');

        Schema::dropIfExists('weekdays');
        Schema::dropIfExists('hours');
        Schema::dropIfExists('frequency');
    }
}
