<?php

namespace App\Mail;

use App\Models\Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FormEnabled extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $url = Config::where('key', '=', 'url')->first();
        return $this
            ->from('admin@admin.com', 'Форма доступна')
            ->subject('Visa application')
            ->text('emails.formEnabled', [
            'url' => $url
        ]);
    }
}
