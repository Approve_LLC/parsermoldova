<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class ParseServiceProvider
 * @package App\Providers
 */
class ParseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\ParseService'
        );
    }
}
