
<?php
	$value = data_get($entry, $column['name']);
?>

<span><a href="mailto:<?php echo e($entry->{$column['name']}); ?>"><?php echo e(str_limit(strip_tags($value), array_key_exists('limit', $column) ? $column['limit'] : 254, "[...]")); ?></a></span>

<?php /* /home/cybtonix/public_html/vendor/backpack/crud/src/resources/views/columns/email.blade.php */ ?>