<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
{{--<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>--}}
{{--<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>--}}
<li><a href='{{ backpack_url('config') }}'><i class='fa fa-tag'></i> <span>Config</span></a></li>
<li><a href='{{ backpack_url('emailreceiver') }}'><i class='fa fa-tag'></i> <span>Email Receiver</span></a></li>
<li><a href='{{ backpack_url('schedule-config') }}'><i class='fa fa-tag'></i> <span>Schedule Config</span></a></li>

{{--<li><a href='{{ backpack_url('weekday') }}'><i class='fa fa-tag'></i> <span>Weekday</span></a></li>--}}
{{--<li><a href='{{ backpack_url('hour') }}'><i class='fa fa-tag'></i> <span>Hour</span></a></li>--}}
{{--<li><a href='{{ backpack_url('frequncy') }}'><i class='fa fa-tag'></i> <span>Frequency</span></a></li>--}}
