<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ScheduleConfigRequest as StoreRequest;
use App\Http\Requests\ScheduleConfigRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ScheduleConfigCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ScheduleConfigCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\ScheduleConfig');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/schedule-config');
        $this->crud->setEntityNameStrings('scheduleconfig', 'schedule_configs');

//        $this->crud->modifyField('weekdays', [
//            'label'     => 'Weekdays',
//            'type'      => 'checklist',
//            'name'      => 'weekdays',
//           // 'entity'    => 'weekdays',
//            'attribute' => 'weekdays',
//            'model'     => "App\Models\ScheduleConfig",
//       //     'pivot'     => true,
//        ]);
//        $this->crud->addField([
//            'name' => 'weekdays',
//            'label' => 'Weekdays',
//            'entity'    => [],
//     //       'attribute' => 'weekdays',
//            'model'     => "App\Models\ScheduleConfig",
//            'type' => 'checklist',
//        ]);

//        $this->crud->modifyField('hours', [
//            'label'     => 'Hours',
//            'type'      => 'checklist',
//            'name'      => 'hours',
//            'entity'    => 'hours',
//            'attribute' => 'hours',
//            'model'     => "App\Models\ScheduleConfig",
//            'pivot'     => true,
//        ]);
//
//        $this->crud->modifyField('frequency', [
//            'label'     => 'Frequency',
//            'type'      => 'checklist',
//            'name'      => 'frequency',
//            'entity'    => 'frequency',
//            'attribute' => 'frequency',
//            'model'     => "App\Models\ScheduleConfig",
//            'pivot'     => true,
//        ]);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

//        $this->crud->addField([
//            'name' => 'mon',
//            'label' => 'Понедельник',
//            'type' => 'checkbox',
//            'value' => 1
//        ]);
//        $this->crud->addField([
//            'name' => 'thu',
//            'label' => 'Вторник',
//            'type' => 'checkbox',
//            'value' => 2
//        ]);
//        $this->crud->addField([
//            'name' => 'wed',
//            'label' => 'Среда',
//            'type' => 'checkbox',
//        ]);
//        $this->crud->addField([
//            'name' => 'tue',
//            'label' => 'Четверг',
//            'type' => 'checkbox',
//        ]);
//        $this->crud->addField([
//            'name' => 'fri',
//            'label' => 'Пятница',
//            'type' => 'checkbox',
//        ]);
//        $this->crud->addField([
//            'name' => 'sat',
//            'label' => 'Суббота',
//            'type' => 'checkbox',
//        ]);
//        $this->crud->addField([
//            'name' => 'sun',
//            'label' => 'Воскресенье',
//            'type' => 'checkbox',
//        ]);
        //$this->crud->removeField('weekdays');
        $this->crud->addField([
            'label'     => 'Дни недели',
            'type'      => 'checklist',
            'name'      => 'weekdays',
            'entity'    => 'weekdays',
            'attribute' => 'name',
            'value' => collect([1, 2, 3, 4, 5, 6, 7]),
            'model'     => "App\Models\Weekday",
            'pivot'     => true,
        ]);
        $this->crud->addField([
            'label'     => 'Дни недели',
            'type'      => 'checklist',
            'name'      => 'hours',
            'entity'    => 'hours',
            'attribute' => 'name',
            'value' => collect([
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16,
                17, 18, 19, 20,
                21, 22, 23
            ]),
            'model'     => "App\Models\Hour",
            'pivot'     => true,
        ]);
        $this->crud->addField([
            'label'     => 'Дни недели',
            'type'      => 'checklist',
            'name'      => 'frequency',
            'entity'    => 'frequency',
            'attribute' => 'name',
            'value' => collect([1, 2, 3, 4, 5]),
            'model'     => "App\Models\Frequency",
            'pivot'     => true,
        ]);
        // add asterisk for fields that are required in ScheduleConfigRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->removeFields(['name']);

        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
