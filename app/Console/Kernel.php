<?php

namespace App\Console;

use App\Models\ScheduleConfig;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Schema;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\ParseSite',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (Schema::hasTable('schedule_config')) {
            $scheduleConfig = ScheduleConfig::where('name', '=', 'main')->first();
            if (!empty($scheduleConfig->weekdays)) {
                $weekdays = $scheduleConfig->weekdays()->get();
                $weekdaysArr = [];
                foreach ($weekdays as $weekday) {
                    $weekdaysArr[] = $weekday->id;
                }
                $schedule = $schedule->command('parse:site');

                if (!empty($weekdaysArr)) {
                    $schedule = $schedule->days($weekdaysArr);
                }

                $frequency = $scheduleConfig->frequency;
                foreach ($frequency as $data) {
                    if ($data->id == 1) {
                        $schedule = $schedule->everyThirtyMinutes();
                    }
                    if ($data->id == 2) {
                        $schedule = $schedule->everyFifteenMinutes();
                    }
                    if ($data->id == 3) {
                        $schedule = $schedule->everyTenMinutes();
                    }
                    if ($data->id == 4) {
                        $schedule = $schedule->everyFiveMinutes();
                    }
                    if ($data->id == 5) {
                        $schedule = $schedule->everyMinute();
                    }
                }

                $schedule = $schedule->timezone($scheduleConfig->timezone);
                $hours = $scheduleConfig->hours()->get();
                foreach ($hours as $hour) {
                    $schedule = $schedule->at($hour->name);
                }
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
