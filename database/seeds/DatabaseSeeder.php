<?php

use App\Models\Config;
use App\User;
use Illuminate\Database\Seeder;
use App\Models\ScheduleConfig;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $url = new Config();
        $url->name = 'Адрес сайта';
        $url->key = 'url';
        $url->value = 'https://secure2.e-konsulat.gov.pl/Informacyjne/Placowka.aspx?IDPlacowki=212';
        $url->save();

        $selector = new Config();
        $selector->name = 'Критерий доступности формы(элемент сайта, который надо проверить)';
        $selector->key = 'selector';
        $selector->value = 'Wiza';
        $selector->save();

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@gmail.com';
        $user->password = '$2y$12$sTr8vVmmU7nNw1s.rxNumewgi3HifQZOfvXMKwEeAQ5RGVGt9t98m';
        $user->save();

        $scheduleConfig = new ScheduleConfig();
        $scheduleConfig->name = 'main';
        $scheduleConfig->timezone = 'Asia/Krasnoyarsk';
        $scheduleConfig->save();

        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Понедельник';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);
        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Вторник';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);
        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Среда';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);
        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Четверг';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);
        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Пятница';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);
        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Суббота';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);
        $weekday = new \App\Models\Weekday();
        $weekday->name = 'Воскресенье';
        $weekday->save();
        $scheduleConfig->weekdays()->save($weekday);

        $weekday = new \App\Models\Hour();
        $weekday->name = '00:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '01:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '02:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '03:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '04:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '05:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '06:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '07:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '09:00';
        $weekday->save();
        
        $weekday = new \App\Models\Hour();
        $weekday->name = '10:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '11:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '12:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '13:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '14:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '15:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '16:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '17:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '18:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '19:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '20:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '21:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '22:00';
        $weekday->save();

        $weekday = new \App\Models\Hour();
        $weekday->name = '23:00';
        $weekday->save();

        $weekday = new \App\Models\Frequency();
        $weekday->name = 'Каждые 30 минут';
        $weekday->save();

        $weekday = new \App\Models\Frequency();
        $weekday->name = 'Каждые 15 минут';
        $weekday->save();

        $weekday = new \App\Models\Frequency();
        $weekday->name = 'Каждые 10 минут';
        $weekday->save();

        $weekday = new \App\Models\Frequency();
        $weekday->name = 'Каждые 5 минут';
        $weekday->save();

        $weekday = new \App\Models\Frequency();
        $weekday->name = 'Каждую минуту';
        $weekday->save();
        
    }
}
