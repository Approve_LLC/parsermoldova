<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class EmailReceiver extends Model
{
    use CrudTrait;

    protected $table = 'email_receivers';

    protected $fillable = ['name', 'email'];

    protected $guarded = ['id'];
}
